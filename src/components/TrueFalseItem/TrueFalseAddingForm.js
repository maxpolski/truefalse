import React, { Component, PropTypes } from 'react';
import linkState from 'react-link-state';

import TrueFalseSwitcher from './TrueFalseSwitcher'

export default class TrueFalseAddingForm extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      trueFalseStatus: false,
      text: ""
    }
  }

  render() {
    const {
      isShown,
      toggleAddingState,
      addTrueFalseItem
    } = this.props;

    const {
      trueFalseStatus
    } = this.state;

    return (
      <div>
        <form style = {{display: isShown ? "block" : "none"}} >
          <div className="form-group">
            <label>True/False status</label>
            <TrueFalseSwitcher
              isSwitchedOn = { trueFalseStatus }
              switchTrueFalseStatus = { this.switchTrueFalseStatus.bind(this) }
            />
          </div>
          <input type="text" valueLink={linkState(this, 'text')} />
          <br />
          <button
            type="button"
            className="btn btn-primary"
            onClick = { this.checkAndAddItem.bind(this) }
          >
            Save
          </button>
          <button
            type="button"
            className="btn btn-default"
            onClick = { toggleAddingState }
          >
            Cancel
          </button>
        </form>
      </div>
    )
  }

  checkAndAddItem(e) {
    const {
      trueFalseStatus,
      text
    } = this.state;

    if(text) {
      this.props.addTrueFalseItem({trueFalseStatus, text})
      this.setState({
        trueFalseStatus: false,
        text: ""
      })
    }
  }

  switchTrueFalseStatus(e) {
    this.setState({
      trueFalseStatus: !this.state.trueFalseStatus
    })
  }

}

TrueFalseAddingForm.propTypes = {
  isShown: PropTypes.bool.isRequired,
  // toggleAddingState: PropTypes.func.idRequired,
};
