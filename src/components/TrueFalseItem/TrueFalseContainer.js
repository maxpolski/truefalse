import React, { Component, PropTypes } from 'react';

import TrueFalseItem from './TrueFalseItem';
import TrueFalseAddingForm from './TrueFalseAddingForm';

export default class TrueFalseContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isAddingFormShown: false,
      trueFalseItems: this.props.trueFalseItems
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('!!!recieved!!!!');
  //   if(nextProps.trueFalseItems !== this.props.trueFalseItems) {
  //     this.setState({
  //       trueFalseItems: props.trueFalseItems
  //     })
  //   }
  // }

  // componentWillMount() {
  //   this.setState({
  //     trueFalseItems: this.props.trueFalseItems
  //   })
  // }

  render() {
    const {
      trueFalseItems
    } = this.props;

    const {
      addTrueFalseItem,
      switchTrueFalseItem
    } = this.props.actions;

    const {
      isAddingFormShown
    } = this.state;

    return (
      <div className="true-false-container">
          Enter items*
          <ul className="list-group">
            {
              trueFalseItems.map((trueFalseItem, index) => {
                return (
                  <TrueFalseItem
                    key = { index }
                    num = { index }
                    isTrue = { trueFalseItem.trueFalseStatus }
                    text   = { trueFalseItem.text }
                    switchTrueFalseItem = { switchTrueFalseItem }
                  />
                )
              })
            }
          </ul>

          { !trueFalseItems.length ? "There is nothing to show" : "" }

          <br />

          <TrueFalseAddingForm
            isShown = { isAddingFormShown  }
            toggleAddingState = { this.toggleAddingState.bind(this) }
            addTrueFalseItem  = { addTrueFalseItem }
          />

          <button
            style = {{ display: isAddingFormShown ? "none" : "block" }}
            type="button"
            className="btn btn-primary"
            onClick={ this.toggleAddingState.bind(this) }
          >
            Add Item
          </button>
      </div>
    );
  }

  toggleAddingState(e) {
    this.setState({
      isAddingFormShown: !this.state.isAddingFormShown
    })
  }
}

TrueFalseContainer.propTypes = {
  trueFalseItems: PropTypes.array.isRequired,
};
