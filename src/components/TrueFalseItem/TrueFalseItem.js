import React, { Component, PropTypes } from 'react';

import TrueFalseSwitcher from './TrueFalseSwitcher';

export default class TrueFalseItem extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      isTrue,
      text,
      switchTrueFalseItem,
      num
    } = this.props;

    return (
      <li className="list-group-item">
        <TrueFalseSwitcher
          switchTrueFalseItem = { switchTrueFalseItem }
          isSwitchedOn = { isTrue }
          parentIndex = { num }
        />
        <span>{ text }</span>
      </li>
    )
  }
}

TrueFalseItem.propTypes = {
  isTrue: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  num: PropTypes.number.isRequired
};
