import { ADD_TRUE_FALSE_ITEM, SWITCH_TRUE_FALSE_ITEM } from '../constants/ActionTypes';

export default function counter(state = [], action) {
  switch (action.type) {
    case ADD_TRUE_FALSE_ITEM:
      let newState = state.slice();
      newState.push(action.trueFalseItem);
      return newState;
    case SWITCH_TRUE_FALSE_ITEM:
      const index = action.trueFalseItemIndex;
      newState = state.slice();
      // newState.slice(index, 1);
      newState[index].trueFalseStatus = !newState[index].trueFalseStatus;
      return newState;
    default:
      return state;
  }
}
