import { ADD_TRUE_FALSE_ITEM, SWITCH_TRUE_FALSE_ITEM } from '../constants/ActionTypes';

export function addTrueFalseItem(trueFalseItem) {
  return {
    type: ADD_TRUE_FALSE_ITEM,
    trueFalseItem
  };
}

export function switchTrueFalseItem(trueFalseItemIndex) {
  return {
    type: SWITCH_TRUE_FALSE_ITEM,
    trueFalseItemIndex
  };
}

// export function incrementIfOdd() {
//   return (dispatch, getState) => {
//     const { counter } = getState();
//
//     if (counter % 2 === 0) {
//       return;
//     }
//
//     dispatch(increment());
//   };
// }
//
// export function incrementAsync() {
//   return dispatch => {
//     setTimeout(() => {
//       dispatch(increment());
//     }, 1000);
//   };
// }
